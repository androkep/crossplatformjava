package com.company;

import java.util.Random;

public class LinearSystemsGenerator {
    private Random random;

    public LinearSystemsGenerator() {
        random = new Random();
    }

    public LinearSystem Generate(int nodes, float density, int minWeight, int maxWeight)
    {
        LinearSystem result = new LinearSystem();
        for (int y = 0; y < nodes; ++y) {
            String name = "N" + y;
            result.addElement(name);
        }

        for (int y = 0; y < nodes; ++y) {
            for (int x = y + 1; x < nodes; ++x) {
                float value = random.nextFloat();
                if (value <= density) {
                    int weight = minWeight + random.nextInt(maxWeight - minWeight);
                    result.addPath(y, x, weight);
                }
            }
        }
        return result;
    }
}
