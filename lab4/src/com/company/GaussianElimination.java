package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class GaussianElimination {
    public LinearSystem SearchMST(LinearSystem linearSystem) {
        LinearSystem result = new LinearSystem();
        for (LinearSystem.Element element : linearSystem.elements) {
            try {
                result.addElement(element.Name);
            }
            catch (Exception e) {

            }
        }

        List<Integer> visitedNodes = new ArrayList<>();
        List<LinearSystem.Path> availablePaths = new ArrayList<>();

        LinearSystem.Element curElement = linearSystem.elements.get(0);
        visitedNodes.add(curElement.Id);

        while (visitedNodes.size() < result.elements.size()){
            availablePaths.addAll(curElement.paths.values());
            availablePaths.removeIf(new Predicate<LinearSystem.Path>() {
                @Override
                public boolean test(LinearSystem.Path path) {
                    return visitedNodes.contains(path.DestinationIndex);
                }
            });

            int bestDistance = 999999;
            LinearSystem.Path bestPath = null;
            for (LinearSystem.Path path : availablePaths)
            {
                if (path.Weight < bestDistance)
                {
                    bestDistance = path.Weight;
                    bestPath = path;
                }
            }

            result.addPath(bestPath.SourceIndex, bestPath.DestinationIndex, bestDistance);
            curElement = linearSystem.elements.get(bestPath.DestinationIndex);
            visitedNodes.add(curElement.Id);
        }

        return result;
    }
}
