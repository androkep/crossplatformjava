package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinearSystem {
    class Element {
        public int Id;
        public String Name;
        public final Map<Integer, Path> paths;

        public Element(int id, String name) {
            Id = id;
            Name = name;
            paths = new HashMap<>();
        }

        public void addPath(Path path) {
            paths.put(path.DestinationIndex, path);
        }

        public boolean pathExists(int dstIndex) {
            return paths.containsKey(dstIndex);
        }

        public int distanceTo(int dstIndex) {
            Path path = paths.get(dstIndex);
            return path.Weight;
        }
    }

    class Path {
        public int SourceIndex, DestinationIndex;
        public int Weight;

        public Path(int srcIndex, int dstIndex, int weight) {
            SourceIndex = srcIndex;
            DestinationIndex = dstIndex;
            Weight = weight;
        }
    }

    public final List<Element> elements;
    private final Map<String, Integer> nodeNamesToIndex;

    public int[][] weightMatrix;

    public LinearSystem() {
        elements = new ArrayList<>();
        nodeNamesToIndex = new HashMap<>();
    }

    public Element getNode(String name) {
        int index = nodeNamesToIndex.get(name);
        return elements.get(index);
    }

    public int addElement(String name) {
        int newId = elements.size();
        Element element = new Element(newId, name);
        elements.add(element);
        nodeNamesToIndex.put(name, newId);
        return newId;
    }

    public void addPath(String srcName, String dstName, int weight) {
        int srcIndex = nodeNamesToIndex.get(srcName);
        int dstIndex = nodeNamesToIndex.get(dstName);
        addPath(srcIndex, dstIndex, weight);
    }

    public void addPath(int srcIndex, int dstIndex, int weight) {
        Path pathSrcToDst = new Path(srcIndex, dstIndex, weight);
        Path pathDstToSrc = new Path(dstIndex, srcIndex, weight);
        Element src = elements.get(srcIndex);
        Element dst = elements.get(dstIndex);
        src.addPath(pathSrcToDst);
        dst.addPath(pathDstToSrc);
    }

    public boolean pathExists(String srcName, String dstName) {
        int srcIndex = nodeNamesToIndex.get(srcName);
        int dstIndex = nodeNamesToIndex.get(dstName);
        return pathExists(srcIndex, dstIndex);
    }

    public boolean pathExists(int srcIndex, int dstIndex) {
        Element src = elements.get(srcIndex);
        return src.pathExists(dstIndex);
    }

    public void generateWeightMatrix() {
        int count = elements.size();
        weightMatrix = new int[count][count];

        for (int y = 0; y < count; ++y) {
            Element src = elements.get(y);
            for (int x = 0; x < count; ++x) {
                if (y == x) {
                    weightMatrix[y][x] = 0;
                    continue;
                }

                if (src.pathExists(x)) {
                    int weight = src.distanceTo(x);
                    weightMatrix[y][x] = weight;
                }
                else weightMatrix[y][x] = -1;
            }
        }
    }
}
