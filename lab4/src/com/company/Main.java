package com.company;

public class Main {
    public static void main(String[] args)
    {
        MyWindow window = new MyWindow("Lab4");
        window.setVisible(true);
    }

    static void printGraphMatrix(LinearSystem linearSystem)
    {
        int[][] matrix = linearSystem.weightMatrix;
        int size = matrix.length;

        System.out.printf("%-4s", "");
        for(int x = 0; x < size; ++x) {
            LinearSystem.Element element = linearSystem.elements.get(x);
            System.out.printf("%-4s", element.Name);
        }
        System.out.println();

        for(int y = 0; y < size; ++y) {
            LinearSystem.Element element = linearSystem.elements.get(y);
            System.out.printf("%-4s", element.Name);
            for(int x = 0; x < size; ++x) {
                int weight = matrix[y][x];
                if (weight == -1) {
                    System.out.printf("%-4s", ".");
                }
                else {
                    System.out.printf("%-4d", matrix[y][x]);
                }
            }
            System.out.println();
        }
    }
}
