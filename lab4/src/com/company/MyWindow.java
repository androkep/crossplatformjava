package com.company;



import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MyWindow extends JFrame {
    final int threadCount = 4;
    Thread[] threads;
    LinearSystemsGenerator generator;

    JButton Button_Start;
    JButton Button_Compare;
    JLabel[] Label_Thread;
    Timer timer;

    public MyWindow(String title)
    {
        super(title);
        threads = new Thread[threadCount];
        generator = new LinearSystemsGenerator();

        JFrame.setDefaultLookAndFeelDecorated(true);
        super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        super.setPreferredSize(new Dimension(300, 200));

        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(6, 1));

        Label_Thread = new JLabel[threadCount];
        for (int i = 0; i < threadCount; ++i)
        {
            threads[i] = new MyThread(i);
            Label_Thread[i] = new JLabel("Thread" + i);
            panel.add(Label_Thread[i]);
        }
        Button_Start = new JButton("Start");
        Button_Start.addActionListener(new Button_Start_Listener());
        panel.add(Button_Start);
        Button_Compare = new JButton("Compare");
        Button_Compare.addActionListener(new Button_Compare_Listener());
        panel.add(Button_Compare);

        super.setContentPane(panel);
        super.pack();

        timer = new Timer(500, new Timer_Listener());
        timer.setRepeats(true);
        timer.start();
    }

    void updateLabels()
    {
        for (int i = 0; i < threadCount; ++i)
        {
            Thread.State state = threads[i].getState();
            int priority = threads[i].getPriority();
            Label_Thread[i].setText(
                    "Thread " + i +
                    ": " + state.toString() +
                    " (" + priority+")");
        }
    }

    class Timer_Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            updateLabels();
        }
    }

    class Button_Start_Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println("Starting all threads!");

            for (int i = 0; i < threadCount; ++i)
            {
                threads[i].start();
            }
        }
    }

    class Button_Compare_Listener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println("Comparing time!");

            long startTime = System.nanoTime();

            for (int i = 0; i < threadCount-1; ++i)
            {
                LinearSystem linearSystem = generator.Generate(600, 0.5f, 3, 20);
                GaussianElimination search = new GaussianElimination();
                LinearSystem result = search.SearchMST(linearSystem);
            }
            long endTime = System.nanoTime();

            long noThreadDuration = (endTime - startTime);

            startTime = System.nanoTime();
            for (int i = 0; i < threadCount; ++i)
            {
                threads[i].start();
            }
            for (int i = 0; i < threadCount; ++i) {
                while (!threads[i].getState().toString().equals("TERMINATED")) {
                    continue;
                }
            }

            endTime = System.nanoTime();
            long multiThreadDuration = (endTime - startTime);
            System.out.println("Час одиного потоку = "+ noThreadDuration);
            System.out.println("Час "+threadCount+ " потоків = " + multiThreadDuration);
            String strOut = noThreadDuration < multiThreadDuration?"1 потік швидше":"1 потік повільніше";
            strOut = noThreadDuration == multiThreadDuration ?"одинаково": strOut;
            System.out.println(strOut);
        }
    }

    class MyThread extends Thread {
        int id;

        @Override
        public void run() {
            System.out.println("Thread " + id + " started successfully");
            LinearSystem linearSystem = generator.Generate(600, 0.5f, 3, 20);
            System.out.println("Thread " + id + " matrix generated. solving");
            GaussianElimination search = new GaussianElimination();
            LinearSystem result = search.SearchMST(linearSystem);
            System.out.println("Thread " + id + " found solution");
            System.out.println("Thread " + id + " terminated");
        }

        public MyThread(int id) {
            super();
            this.id = id;
        }
    }
}
