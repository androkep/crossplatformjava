package com.company;
import java.util.*;
public class Main {
    public static void main(String[] args) {
        Dictionary manager = new Dictionary();
        int option = 0;
        do {
            System.out.println("1 - Dictionary analysis \n" +
                            "2 - Find and replace\n" +
                            "3 - Sort words \n" +
                            "4 - Exit \n");
            Scanner in = new Scanner(System.in);
            option = in.nextInt();
            in.nextLine();
            switch(option){
                case 1:
                    manager.splitWordsInParts();

                    break;
                case 2:
                    System.out.println(manager.getText());

                    System.out.println("Word to replace:");
                    String word = in.next();
                    System.out.println(manager.makeReplacement(word));
                    break;


                case 3:
                    Text text = new Text();
                    System.out.println("Write sentences to sort:");

                    String textString = in.nextLine();

                    text.setText(textString);
                    System.out.println("Words starting with a vowel:");

                    List<String> words = text.getVowelWords();
                    for (String w : words) {
                        System.out.println(w);
                    }

                    words = text.sortByConsonant(words);
                    System.out.println("Sorted by first consonant:");
                    for (String w : words) {
                        System.out.println(w);
                    }
                    break;
                case 4:
                    break;
                default:
                    System.out.println("Please, enter a nubmer between 1 and 4\n");
                    break;
            }
        }while(option != 4);
    }
}