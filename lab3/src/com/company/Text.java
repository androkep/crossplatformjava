package com.company;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
public class Text {
    private String text;
    public void setText(String text) {
        this.text = text;
    }
    public List<String> getVowelWords() {
        List<String> words = Arrays.stream(text.split("\\s"))
                .filter(word -> word.toLowerCase()
                        .matches("[aeiou].*"))
                .collect(Collectors.toList());
        return words;
    }
    public List<String> sortByConsonant(List<String> words) {
        Collections.sort(words, new ConsComparator());
        return words;
    }
    private static class ConsComparator implements Comparator<String> {
        @Override
        public int compare(String s1, String s2) {
            s1 = s1.toLowerCase().replaceAll("[aeiou]", "");
            s2 = s2.toLowerCase().replaceAll("[aeiou]", "");
            return s1.compareTo(s2);
        }
    }
}