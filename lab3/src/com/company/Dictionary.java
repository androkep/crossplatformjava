package com.company;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.BreakIterator;
import java.util.*;
import java.util.Scanner;
public class Dictionary {
    Set<String> dictionary = new TreeSet<String>();
    String text;
    public String getText(){
        return text;
    }
    public Dictionary(){
        fillDictionary();
        fillText();
    }
    private void fillDictionary()
    {

        File file = new File("dictionary.txt");
        try (Scanner sc = new Scanner(file, StandardCharsets.UTF_8.name())) {
            while (sc.hasNextLine()){

                dictionary.add(sc.nextLine());
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void fillText(){
        try {
            byte[] textBytes = Files.readAllBytes(Paths.get("text.txt"));
            text = new String(textBytes, "UTF-8");
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    public String makeReplacement(String word) {
        List<String> ss = new ArrayList<String>();
        BreakIterator breakIterator =
                BreakIterator.getSentenceInstance(Locale.US);
        breakIterator.setText(text);
        int start = breakIterator.first();
        for (int end = breakIterator.next(); end!= BreakIterator.DONE; start =
                end, end = breakIterator.next()) {
            ss.add(text.substring(start, end));
        }
        String newText = new String();
        for (String sentence : ss) {
            sentence = replaceWord(sentence, word);
            newText += sentence;
        }
        return newText;
    }
    private String replaceWord(String s, String word) {
        String res = "";
        String delim = "+-*/(),. ";
        StringTokenizer tok = new StringTokenizer(s, delim, true);
        while (tok.hasMoreTokens()) {
            String w = tok.nextToken();
            if (w.equals(word)) {
                res = res + getFirstWord(s).toLowerCase();
            } else {
                res = res + w;
            }
        }
        return res;
    }
    private String getFirstWord(String text) {
        int i = text.indexOf(' ');
        if (i > -1) {
            return text.substring(0, i).trim();
        } else {
            return text;
        }
    }
    public void splitWordsInParts() {
        for (String s: dictionary) {
            List<String> found = new ArrayList<>(splitWord(s, dictionary));
            String output = "";
            for (String ss : found) {
                output +=(", " + ss);
            }
            System.out.println( "'"+s +"' => " + output);
        }
    }
    public List<String> splitWord(String s, Set<String> wordDict) {
        int[] pos = new int[s.length()+1];
        List<String> ss = new ArrayList<>();
        Arrays.fill(pos, -1);
        pos[0]=0;
        for(int i=0; i<s.length(); i++){
            if(pos[i]!=-1){
                for(int j=i+1; j<=s.length(); j++){
                    String sub = s.substring(i, j);

                    if(wordDict.contains(sub) && (sub != s) ){

                        ss.add(sub);
                        pos[j]=i;

                    }
                }
            }
        }
        return ss;
    }
}