package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
public class FileModule {

    public List<Device> readFromFile(String pathName) {
        List<Device> readedList = new ArrayList<Device>() ;
        try {
            File myObj = new File(pathName);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                String[] sentence = data.split("\\;+");
                int price  = Integer.parseInt(sentence[3]);
                int power  = Integer.parseInt(sentence[4]);

                readedList.add(new Device(sentence[0],sentence[1],sentence[2],price,power));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }      if(readedList.isEmpty())
            return null;
        else return readedList;
    }

}

