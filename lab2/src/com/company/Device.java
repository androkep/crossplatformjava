package com.company;
import java.util.*;

public class Device {
    public String name;
    public String model;
    public String manufacturer;
    public int price;
    public int power;

    Device(String name, String model, String manufacturer, int price, int power)
    { this.name=name;
        this.model=model;
        this.manufacturer = manufacturer;
        this.price = price;
        this.power = power;
    }
    public String getname()
    {
        return this.name;
    }
    public int getprice()
    {
        return this.price;
    }
    public void printAppliences()
    {
        System.out.println("-----\n"+this.name+"\n"+this.model+"\n"+this.manufacturer +"\n"+this.price+" UAH\n"+this.power +" W");
    }
}

class SortByPrice implements Comparator<Device>
{
    public int compare(Device a, Device b)
    {
        return a.name.compareTo(b.name);
    }
}

class SortByPrice2 implements Comparator<Device>
{
    public int compare(Device a, Device b)
    {
        return a.price - b.price;
    }
}
