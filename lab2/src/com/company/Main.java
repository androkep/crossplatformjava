package com.company;

import java.util.List;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println();
        System.out.println(
                "1 - Create a sorted map with n first items"+"\n" +
                "2 - Count absolute and relative number of occurances" +"\n"+
                "3 - Join 2 files into a map"+"\n"+
                "4 - Print the map "+"\n"+
                "5 - Escape"
        );
        int choice = sc.nextInt();
        FileModule FR = new FileModule();
        List<Device> readedList;
        readedList = FR.readFromFile("devices0.txt");
        DeviceManager AM= new DeviceManager();
        AM.setAppliancesList(readedList);

        AM.createMap();
        while (true) {
            switch (choice) {
                case 1: {
                    Scanner num = new Scanner(System.in);
                    int number = num.nextInt();
                    AM.printFirstNElementMap(number);
                    break;
                }
                case 2: {
                    AM.frequencyresponse();
                    break;
                }
                case 3: {
                    List<Device> readedList2;
                    readedList2 = FR.readFromFile("devices1.txt");

                    DeviceManager MM = new DeviceManager();
                    MM.setAppliancesList(AM.UnitedArray(readedList2));
                    MM.createMap();
                    MM.printMap();
                    break;
                }
                case 4: {
                    AM.printMap();
                    break;
                }
                case 5: {
                    System.exit(0);
                    break;
                }
            }
            choice = sc.nextInt();
        }
    }
}
