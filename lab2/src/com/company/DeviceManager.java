package com.company;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class DeviceManager {
    public TreeMap<String, List<Device>> map = null;
    public List<Device> appliancesList;
    public Iterator<Device> hIter = null;

    public void setAppliancesList(List<Device> appliancesList) {
        this.appliancesList = appliancesList;
        this.hIter = this.appliancesList.iterator();

    }

    public void createMap() {
        TreeMap<String, List<Device>> map = new TreeMap<String, List<Device>>();
        while (hIter.hasNext()) {
            Device appliances = hIter.next();
            if (!map.containsKey(appliances.getname())) {
                List<Device> here = new LinkedList<Device>();
                here.add(appliances);
                map.put(appliances.getname(), here);
            } else {
                map.get(appliances.getname()).add(appliances);
            }
            map.entrySet().forEach(entry->{
                Collections.sort(entry.getValue(), new SortByPrice());});
        }
        if (!map.isEmpty())
            this.map = map;
    }
    public void frequencyresponse()
    {
        int count = this.map.keySet().stream().mapToInt(householdAppliances -> (int) map.get(householdAppliances).stream().count()).sum();
        this.map.entrySet().forEach(entry -> {

            map.get(entry.getKey()).stream().count();


            System.out.println("------\n"+entry.getKey()+" Count: "+map.get(entry.getKey()).stream().count() +" Relative: "+(float)map.get(entry.getKey()).stream().count()/count*100+"%\n");

        });
    }
    public void printMap() {
        System.out.println("++++++++++++++++:");
        System.out.println();

        this.map.entrySet().forEach(entry -> {



            for (Device appliances : entry.getValue()) {

                appliances.printAppliences();
            }
            ;
            System.out.println();

        });
        System.out.println("-------------------------------------------------------------------------------");
    }

    public void printFirstNElementMap(int n) {
        System.out.println("=============================:");
        System.out.println();
        AtomicInteger count = new AtomicInteger();
        this.map.entrySet().forEach(entry -> {


            for (Device appliances : entry.getValue()) {
                if (count.get() == n)
                {break;}
                else {
                    appliances.printAppliences();
                    count.set(count.get() + 1);
                }

            }
            ;


        });
        System.out.println("-------------------------------------------------------------------------------");
    }

    public List<Device> UnitedArray(List<Device> list2) {
        appliancesList.addAll(list2);
        Collections.sort(this.appliancesList, new SortByPrice2());
        int pricee = appliancesList.stream().mapToInt(Device::getprice).sum();
        System.out.println("Price sum: "+pricee);
        return appliancesList;

    }
}


