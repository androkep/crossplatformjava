package com.company;

import java.util.Arrays;


public class Main {
    public static void main(String[] args) {

        ManufacturerModel manufacturer1 = new ManufacturerModel("Germany");
        ManufacturerModel manufacturer2 = new ManufacturerModel("France");

        PartModel engine1 = new PartModel("6L V8", 120000.0, "EG12332342",
                PartType.engine, manufacturer1);
        PartModel transmission1 = new PartModel("4WD 5 speed", 60000.0, "TR02341232",
                PartType.transmission, manufacturer1);
        PartModel engine2 = new PartModel("1.3L I4", 2000.0, "EG02343687",
                PartType.engine, manufacturer1);
        PartModel battery1 = new PartModel("battery 12V 600A", 150.0, "BT00000123",
                PartType.engine, manufacturer2);
        PartModel starter1 = new PartModel("starter 12V 2500W", 100.0, "ST00000435",
                PartType.starter, manufacturer2);
        PartModel transmission2 = new PartModel("RWD 6 speed", 50000.0, "TR02678432",
                PartType.transmission, manufacturer2);
        PartModel generator1 = new PartModel("generator 12V 500W", 300.0, "GN00013735",
                PartType.generator, manufacturer2);
        PartManager manager = new PartManager(Arrays.asList(engine1, transmission1, engine2, battery1,
                starter1, transmission2, generator1));

        manager.print();
        manager.filterByChassis();
        manager.sortByNameAndManufacturer(true);
        manager.sortByManufacturer(false);
        manager.sortBySerial(true);
    }
}
