package com.company;

import java.util.Comparator;
import java.util.List;

public class PartManager {
    List<PartModel> parts;

    public PartManager(List<PartModel> _parts) {
        parts = _parts;
    }

    public List<PartModel> getParts() {
        return parts;
    }

    public void setParts(List<PartModel> parts) {
        this.parts = parts;
    }

    public void filterByChassis() {
        System.out.print("\n*****************************************\n Filter by chassis (engine, transmission):");
        for (var parts : parts) {
            if (parts.getType().toString() == "transmission" ||
                    parts.getType().toString() == "engine")
                System.out.print(parts.getData());
        }
    }

    public void sortByManufacturer(boolean ascending) {
        manufacturerComparator comparator = new manufacturerComparator();
        System.out.print("\n*****************************************\n Sort by manufacturer with comparator as inner class");
        if (ascending) {
            parts.sort(comparator);
        } else {
            parts.sort(comparator.reversed());
        }
        print();
    }

    public void sortBySerial(boolean ascending) {
        System.out.print("\n*****************************************\n Sort by serial with comparator as static inner  class");
        if (ascending) {
            parts.sort(SerialComparator.getInstanse());
        } else {
            parts.sort(SerialComparator.getInstanse().reversed());
        }
        print();
    }

    public void sortByNameAndManufacturer(boolean ascending) {
        System.out.print("\n*****************************************\n Sort by name and manufacturer name with comparator as  anonymous inner class");
        var comparator = new Comparator<PartModel>() {
            @Override
            public int compare(PartModel o1, PartModel o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        manufacturerComparator manufacturerComparator = new manufacturerComparator();
        if (ascending) {
            parts.sort(comparator.thenComparing(manufacturerComparator));
        } else {

            parts.sort(comparator.thenComparing(manufacturerComparator).reversed());
        }
        print();
    }

    public void print() {
        for (var parts : parts) {
            System.out.print(parts.getData());
        }
    }

    public static class SerialComparator implements Comparator<PartModel> {
        private static SerialComparator serialComparator;

        private SerialComparator() {
        }

        public static SerialComparator getInstanse() {
            if (serialComparator == null)
                serialComparator = new SerialComparator();
            return serialComparator;
        }

        public int compare(PartModel o1, PartModel o2) {
            return
                    o1.getSerial_number().compareTo(o2.getSerial_number());
        }
    }

    public class manufacturerComparator implements Comparator<PartModel> {
        @Override
        public int compare(PartModel o1, PartModel o2) {
            return
                    o1.getManufacturer().getName().compareTo(o2.getManufacturer().getName());
        }
    }
}
