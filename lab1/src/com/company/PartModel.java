package com.company;

public class PartModel {
    String name;
    Double price;
    String serial_number;
    PartType type;
    ManufacturerModel manufacturer;

    public PartModel(String _name, Double _price, String _documentation, PartType _type, ManufacturerModel _manufacturer) {
        name = _name;
        price = _price;
        serial_number = _documentation;
        type = _type;
        manufacturer = _manufacturer;
    }

    public String getData() {
        return String.format("\n\nName: " + name +
                "\nType: " + type.name().toString() +
                "\nPrice: " + price +
                "\nSerial: " + serial_number +
                "\nManufacturer: " + manufacturer.name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public PartType getType() {
        return type;
    }

    public void setType(PartType type) {
        this.type = type;
    }

    public ManufacturerModel getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(ManufacturerModel manufacturer) {
        this.manufacturer = manufacturer;
    }
}
