package com.company;

public enum PartType {
    engine,
    transmission,
    generator,
    battery,
    starter
}
