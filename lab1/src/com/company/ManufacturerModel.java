package com.company;

import java.util.List;

public class ManufacturerModel {
    String name;
    List<PartModel> parts;

    public ManufacturerModel(String _name) {
        name = _name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PartModel> getParts() {
        return parts;
    }

    public void setParts(List<PartModel> parts) {
        this.parts = parts;
    }
}
